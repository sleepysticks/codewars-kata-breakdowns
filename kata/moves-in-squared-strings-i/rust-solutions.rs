/// note: calling `join()` on a collection of `&str` returns a `String` because of this trait:
/// https://doc.rust-lang.org/std/slice/trait.SliceConcatExt.html
/// which was pretty confusing for me until I found this.

fn hor_mirror(s: String) -> String {
    /// reverse split (e.g. iterating from back to front) `s` on `'\n'` (single quotes indicate a `char`)
    /// `collect()` into a `Vec<&str>`
    /// `join()` each element of the `Vec<String>` with `"\n"` (double quotes indicate a `&str`)
    /// the call to `join()` returns a `String`, which itself is then returned from `hor_mirror()`
    s.rsplit('\n').collect::<Vec<_>>().join("\n")
}

fn vert_mirror(s: String) -> String {
    /// split `s` on `'\n'` (single quotes indicate a `char`)
    /// map each `&str` segment to the following effect:
    /// * get an iterator of the `char`s in `seg`
    /// * reverse the order of the `char`s (which actually just returns an iterator that iterates backwards through the `char`s)
    /// * `collect()` into a `String`
    /// `collect()` all of those `map`ped `String`s into a `Vec<String>`
    /// `join()` each element of the `Vec<String>` with `"\n"` (double quotes indicate a `&str`)
    /// the call to `join()` returns a `String`, which itself is then returned from `hor_mirror()`
    s.split('\n')
        .map(|seg| seg.chars().rev().collect::<String>())
        .collect::<Vec<_>>()
        .join("\n")
}

fn oper(fct: fn(String) -> String, s: String) -> String {
    fct(s)
}
