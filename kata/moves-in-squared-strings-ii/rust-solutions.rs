fn rot(s: &str) -> String {
    /// get an iterator of the `char`s in `s`
    /// reverse the order of the `char`s (which actually just returns an iterator that iterates backwards through the `char`s)
    /// `collect()` into a `String` (we know it's a `String` because of the function return type)
    s.chars().rev().collect()
}

fn selfie_and_rot(s: &str) -> String {
    /// split `s` on `'\n'` (single quotes indicate a `char`)
    /// map each `&str` segment to a `format!` macro that:
    /// * fills `width` space with the period character (`.`)
    /// * left aligns ('<') the content (`seg`, in this case)
    /// * fills to the `width` (defined by `std::fmt`) provided in parameter 1 (`1$`), which for us is `seg.len() * 2`
    /// * the `format!` macro returns a `String`
    /// `collect()` into a `Vec<String>`
    /// `join()` each element of the `Vec<String>` with `"\n"` (double quotes indicate a `&str`)
    /// all of which is stored in `withDots`, which is now a `String` after the call to `join()`
    /// Reference: https://doc.rust-lang.org/std/fmt/#syntax
    let withDots = s.split('\n')
        .map(|seg| format!("{:.<1$}", seg, seg.len() * 2))
        .collect::<Vec<_>>()
        .join("\n");
    /// concat (with newline) original `String` extended with periods and the exact reverse of that `String`
    /// note that we pass `withDots` to `rot()` with a `&` prefix, which dereferences a `String` to a
    format!("{}\n{}", withDots, rot(&withDots))
}

fn oper(fct: fn(&str) -> String, s: &str) -> String {
    fct(s)
}
