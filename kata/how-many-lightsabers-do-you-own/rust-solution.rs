/// given how simple the conditions for this kata are, an `if` statement is
/// also an equally readable solution, I just personally prefer `match` syntax.
///
/// ```rust
/// if name == "Zach" {
///     18
/// } else {
///     0
/// }
/// ```
///
/// furthermore, `match` is actually safer than `if` conditions because the
/// compiler _enforces_ exhaustive matching. that is, the compiler requires
/// a `match` statement to handle all possible cases, something that isn't
/// enforced for `if`

fn how_many_lightsabers_do_you_own(name: &str) -> u8 {
    /// `match` (similar `switch` in C/C++) on the input provided (`name`)
    /// * if `name` is `Zach` then the `match` statement resolves to `18`
    /// * if `name` is _anything_ else (`_` is the default catch-all, which
    ///   ensures all cases are properly covered) the `match` statement
    ///   resolves to `0`
    /// since the `match` result isn't captured or stored, the result becomes
    /// the `u8` return value for the function
    ///
    /// notice that the return values we give (`0` and `18`) could easily be a
    /// variety of other signed or unsigned integer types, such as `usize`,
    /// `i16`, or `u64`. the rust compiler, given a return type of `u8` and a
    /// value that can easily _be_ a `u8` simply infers that we intend those
    /// values to be of type `u8` instead of requiring us to be extra explicit
    /// and write `18u8`.
    match name {
        "Zach" => 18,
        _ => 0,
    }
}
