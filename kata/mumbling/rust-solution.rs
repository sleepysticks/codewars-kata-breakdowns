/// this `use` is required to access `to_ascii_uppercase()` for `char` (and a few other types) in rust < 1.23
/// in rust 1.23+ these functions reside directly on the `char`, `str`, `u8`, and `[u8]` types
use std::ascii::AsciiExt;

/// required to utilize `once()` (returns an iterator used for chaining a one-off element into another iterator, usually with `chain()`)
/// and `repeat` (an iterator used to repeat an element indefinitely, almost always paired with `take()` to retrieve X of that element)
use std::iter::{once, repeat};

fn accum(s: &str) -> String {
    /// get an iterator of `(index: usize, letter: char)` tuples for `char`s in `s`
    /// map each `(index, letter)` tuple to the following logic:
    /// * use `once()` to get once instance of `letter` as uppercase with `to_ascii_uppercase()`
    /// * use `chain()` to extend the iterator returned by `once()`
    /// * use `repeat()` to create an iterator (used by `chain()` to extend the `once()` iterator) that generates infinite lowercase `letter`s with `to_ascii_lowercase()`
    /// * use `take()` to declare how many elements you want to take (get it?) from the `repeat()` iterator (in this case, `index` happens to be the right number for our problem)
    /// * finally, `collect()` all of those letters into a `String`
    /// `collect()` all of those `String`s into a `Vec<String>`
    /// `join()` each element of the `Vec<String>` with `"-"` (double quotes indicate a `&str`)
    s.char_indices()
        .map(|(index, letter)| {
            once(letter.to_ascii_uppercase())
                .chain(repeat(letter.to_ascii_lowercase()).take(index))
                .collect::<String>()
        })
        .collect::<Vec<_>>()
        .join("-")
}
