# what's codewars?
https://codewars.com is a website dedicated to providing targeted code exercises in many languages to help people "train".

generally the same kata (exercise) can be completed in a decent number of languages, helping programmers new to a certain language tackle it first in a language they better understand.

# so why document solutions?
only some solutions, generally my own, will be documented here.

while codewars is certainly a great way to see how different people solve different problems, rarely are the answers actually documented enough to actually help people understand _why_ they work.

this is probably more a teaching aid for me trying to help my friends/coworkers get into [rust](https://www.rust-lang.org/) (and maybe other languages with time) more than anything else.

# solution structure
all solutions reside in the [kata/](kata) directory.

each dirname in [kata/](kata) can be accessed on codewars with the following link template:

    https://codewars.com/kata/<dirname>
    
for example, the kata [moves-in-squared-strings-i](kata/moves-in-squared-strings-i) could be accessed at:

    https://codewars.com/kata/moves-in-squared-strings-i